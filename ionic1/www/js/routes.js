angular.module('youper')
.config(function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('youper');

    $stateProvider

    .state('youpernotif',{
        url:'/notif',
        templateUrl:'templates/youper-notif.html',
        controller:'YouperNotif'
    })

    .state('youperdetail',{
        url:'/detail',
        templateUrl:'templates/youper-detail.html',
        controller: 'YouperDetail'
    })


})