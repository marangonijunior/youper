angular.module('youper.controllers', [])

    .controller('YouperHome', ['$scope', '$state', function($scope, $state) {

        $scope.photo = null;
        $scope.notifON = false;
        $scope.badge = 0;

        $scope.photoOpen = function(){
            
        }

        $scope.goTo = function(){
            $state.go('youpernotif');
        }

    }])

    .controller('YouperNotif', function ($scope, $stateParams) {
        
    })

    .controller('YouperDetail', function ($scope, $stateParams) {
       
    });